# FORM ubuntu:lastest AS build
# RUN apt-get update
# RUN apt-get install openjdk-17-jdk -y
# COPY . .
# RUN ./gradle bootJar --no-daemon

# FROM openjdk:17-jdk-slim
# EXPOSE 8080
# COPY --from=build /build/libs/demo-1.jar app.jar

# EXTRYPOINT ["java","-jar","aap.jar"]


# FROM eclipse-temurin:17-jdk-alpine
# VOLUME /tmp
# COPY target/*.jar app.jar
# ENTRYPOINT ["java","-jar","/app.jar"]
# EXPOSE 8080

FROM eclipse-temurin:17-jdk-focal
 
WORKDIR /app
 
COPY .mvn/ .mvn
COPY mvnw pom.xml ./
RUN ./mvnw dependency:go-offline
 
COPY src ./src
 
CMD ["./mvnw", "spring-boot:run"]